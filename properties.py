import bpy
import os

from bpy.props import (
    StringProperty, IntProperty, BoolProperty,
    PointerProperty, CollectionProperty,
    FloatProperty, FloatVectorProperty,
    EnumProperty,
)

from bpy.types import (PropertyGroup, UIList,
                       WindowManager, Scene,
                       )
from bpy.utils import (register_class,
                       unregister_class
                       )

def render_engines(self, context):
    # callback for the enumlist of the dynamic enums
    scene = context.scene
    brt_props = scene.brt_props
    items = [('BLENDER_WORKBENCH','Workbench', ''),('CYCLES','Cycles', ''),('BLENDER_EEVEE','Eevee', '')]

    return items

class brt_props(PropertyGroup): 
    '''property class stored with various variables in bpy.context.scene.brt_props''' 
    
    sound_on: BoolProperty(
        name="Include audio from sequencer",
        description="Enable or Disable Sound track from Video Sequencer",
        default=True,
    )   
    simplify_on: BoolProperty(
        name="Use Simplify",
        description="Enable or Disable Simplify ",
        default=False,
    )
    only_selected: BoolProperty(
        name="Only Selected",
        description="Render only selected objects",
        default=False,
    )
    start_frame: IntProperty(
        name="Start frame",
        min = 0,
        description="Temporary start frame for the playblast ",
        default=0,
    )
    end_frame: IntProperty(
        name="End frame",
        description="Temporary start frame for the playblast ",
        min = 0,
        default=100,
    )
    render_percent: IntProperty(
        name="Render percentage",
        min = 1,
        max = 100,
        description="Scaler for the image dimensions, use lower numbers to render quickly",
        default=50,
    )
    renderers: EnumProperty(
        name="Render Engine",
        description="Render Engine to be used for the playblast",
        items=render_engines
    )
    enable_copy: BoolProperty(
        name="Copy to dir:",
        description="Copies the render result to another folder",
        default=False,
    )
    copy_dir: StringProperty(
        name="Copy to dir",
        description="Folder used to copy the render result",
        subtype='DIR_PATH',
        default=os.path.expanduser('~') + os.path.sep,
    )
    cycles_samples_render: IntProperty(
        name="Render samples",
        min = 0,
        description="Temporary Cycles render samples settings for the playblast ",
        default=50,
    )
    eevee_samples_render: IntProperty(
        name="Render samples",
        description="Temporary Eevee render samples settings for the playblast ",
        min = 0,
        default=16,
    )
    global_ao:  BoolProperty(
        name="Ambient occlusion",
        description="Enable Ambient occlusion for Eevee",
        default=False,
    )
    use_stamps: BoolProperty(
        name="Copy to dir:",
        description="Burn stamps in the render",
        default=False,
    )


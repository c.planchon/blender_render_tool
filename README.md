# Blender Render Tool
Blender addon mainly focused on rendering the scene animation without modifying the current render settings.
<br>Please report bugs via [Issues > Boards](../../boards) in the left sidebar. 
 
## Installation
Download from this repository
```
git clone git@gitlab.com:c.planchon/blender_render_tool.git
```
then install it via the Add-ons tab in the Blender preferences window or copy the extracted folder to the Blender addons directory and activate it from the Preferences panel.
 
 
## Documentation
All the settings of the addon are set only for the Playblast rendering without altering the scene settings.
Useful for artists who do not wish to change their render configuration just to render a preview.
### Blender Render Tool main panel in Properties > Render
![BRT Main Panel in Properties window](http://cosmin.fr/images/brt_main_panel.jpg "Header button")
 
#### Presets :
Preset list to store and retrieve settings for the Blender Render Tool. 
 
#### Quick playblast :
Main button used to render the animation playblast.
 
#### Start & End frame : 
Frame range to use when rendering the animation playblast.
 
#### Render percentage : 
Render the playblast faster by reducing the percentage of the current render size settings.
 
#### Render engine :
Render engine to be used for rendering the playblast.
 
#### Options :
 
##### Include Audio from sequencer :
If audio tracks are present in the video sequencer they will be added to the playblast with this option.
 
##### Use simplify :
Enables simplify and set the subdivision level to 0 during the playblast rendering time.
 
##### Only selected :
If this option is enabled only the selected objects will be rendered during the playblast.
 
##### Burn Stamps :
Add a stamp with the frame number on the playblast. Additional parameters are available in the native Blender stamps panel.  
 
##### Copy to :
Copies the rendered preview.mov to the chosen folder after rendering the playblast. 
 
### Additional Playblast button in the 3D view header menu
 
![Brt button in the 3D view header menu](http://cosmin.fr/images/brt_top_menu.jpg "BRT Header")
<br>
<em>A Playblast button is also available from the 3D view header menu</em>
 
 


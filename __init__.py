bl_info = {
    "name": "Blender Render Tool",
    "author": "Cosmin Planchon",  
    "version": (1, 0, 1),
    "blender": (2, 90, 0),
    "location": "Properties > Render",
    "description": "Playblast tool to render animations quickly with custom parameters and various options.",
    "warning": "",
    "wiki_url": "https://gitlab.com/c.planchon/blender_render_tool",
    "tracker_url": "https://gitlab.com/c.planchon/blender_render_tool/-/issues",
    "category": "Animation"}

import bpy

from bpy.props import (
    StringProperty, IntProperty, BoolProperty,
    PointerProperty, CollectionProperty,
    FloatProperty,FloatVectorProperty,
    EnumProperty,
)

from . import properties

from . import operators

from . import panels

from . properties import (brt_props )

from . operators import (BRT_OT_playblast , BRT_OT_addpreset ,
                         BRT_ExecutePreset, BRT_MT_presetsmenu, panel_func
                         )

from . panels import (BRT_PT_panel, BRT_PT_buttons, BRT_PT_options, 
                      BRT_MT_menu_top, brt_menu_top, BRT_PT_cycles_options, BRT_PT_tab,
                      BRT_PT_eevee_options, BRT_PT_global_options,
                      )

classesp = (brt_props, BRT_OT_playblast, BRT_PT_tab, BRT_PT_panel, BRT_ExecutePreset,
            BRT_PT_buttons, BRT_PT_options, BRT_OT_addpreset, BRT_MT_presetsmenu,
            BRT_PT_cycles_options, BRT_PT_eevee_options, 
            BRT_PT_global_options, BRT_MT_menu_top
            )

def register():
    '''mandatory to register addon classes when enabling the addon'''
    from bpy.utils import register_class
    for cls in classesp:
        register_class(cls)

    bpy.types.Scene.brt_props = PointerProperty(type=brt_props)
    bpy.types.VIEW3D_MT_editor_menus.append(brt_menu_top)
    bpy.types.BRT_PT_panel.prepend(panel_func)

def unregister():
    '''mandatory to unregister addon classes when
       disabling addon in reverse order than register'''
    bpy.types.BRT_PT_panel.remove(panel_func)
    bpy.types.VIEW3D_MT_editor_menus.remove(brt_menu_top)
    
    from bpy.utils import unregister_class
    for cls in classesp:
        unregister_class(cls)
    del bpy.types.Scene.brt_props

if __name__ == '__main__':
    register()
